sampleApp.controller('HomeController', function($scope) {
	$scope.showLoginButton = !Boolean(localStorage.getItem("isLoggedIn"));
    $scope.name = "Guest";
    window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
        FB.init({
        appId      : '1330167790435760', // FB App ID
        cookie     : true,  // enable cookies to allow the server to access the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
        });
        
        // Check whether the user already logged in
        FB.getLoginStatus(function(response) {
            console.log("response - ",response);
            if (response.status === 'connected') {
                //display user data
                localStorage.setItem("isLoggedIn",true);
                $scope.showLoginButton = false;
				localStorage.setItem("accessToken",response.authResponse.accessToken);
				localStorage.setItem("userId",response.authResponse.userID);
                getFbUserData();
            } else {
                console.log("status is unknown")
                localStorage.setItem("isLoggedIn",false);
                $scope.showLoginButton = true;
                console.log("$scope.showLoginButton - ",$scope.showLoginButton)
            }
        });
    };


    $scope.fbLogin = function() {
        console.log("in fb login");
            FB.login(function (response) {
                if (response.authResponse) {
                        // Get and display the user profile data
                        getFbUserData();
                } else {
                        alert("Problem in Login");
                }
            }, {scope: 'email'});
    }

     // Fetch the user profile data from facebook
    function getFbUserData(){
        FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
        function (response) {
            console.log("response is - ",JSON.stringify(response));
            $scope.name = response.first_name + " " + response.last_name;
            console.log(" $scope.name - ", $scope.name);
        });
    }
	
});

