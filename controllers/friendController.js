sampleApp.controller('FriendController', function($scope,$http) {
	
    function calculateFriends() {
        console.log("access token is - ",localStorage.getItem("accessToken"))
        console.log("userId is - ",localStorage.getItem("userId"))
        var accessToken = localStorage.getItem("accessToken");
        var userId = localStorage.getItem("userId");
        var url = "https://graph.facebook.com/v1.0/"+ userId + "/taggable_friends?fields=name,id,picture&limit=500&offset=0&access_token=" + accessToken;
        console.log("url is - ",url);
        $scope.topFriends = [];
        $http.get(url).then(function(response) {
            // console.log("response of friends are - ",JSON.stringify(response));
            if(response.status==200) {
                var friendsArray = response.data.data;
                // console.log("firnds array is - ",JSON.stringify(friendsArray));
                var friendsCount = friendsArray.length;
                // console.log("friends count is - ",JSON.stringify(friendsCount));
                var indexArray = calulateNRandomNumbers(5,friendsCount-1);
                console.log("index array is - ",JSON.stringify(indexArray));
                $scope.topFriends[0] = friendsArray[indexArray[0]];
                $scope.topFriends[1] = friendsArray[indexArray[1]];
                $scope.topFriends[2] = friendsArray[indexArray[2]];
                $scope.topFriends[3] = friendsArray[indexArray[3]];
                $scope.topFriends[4] = friendsArray[indexArray[4]];

                console.log("top 5 friends are - ",JSON.stringify($scope.topFriends));

            } else {
                alert("Error in getting friends pls try after some time");
            }
        });
    }
    calculateFriends();


    function calulateNRandomNumbers(n,range) {
        console.log("n and range is - ",n,range);
        var nums = [];
        while(nums.length<n) {
            var temp = Math.round(range*Math.random());
            var flag=0;
            console.log("temp is - ",temp);
            // Check for uniqueness
            for(var i=0; i<nums.length;i++) {
                if(temp == nums[i]) {
                    flag = 1;
                }
            }
            if(!flag) {
                nums.push(temp);
            }
        }
        return nums;
    }
   
});