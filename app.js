//Define an angular module for our app
var sampleApp = angular.module('sampleApp', []);

//Define Routing for app
sampleApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/Home', {
	templateUrl: 'templates/home.html',
	controller: 'HomeController'
      }).
      when('/getTop5Friends', {
	templateUrl: 'templates/topFriends.html',
	controller: 'FriendController'
      }).
      otherwise({
	redirectTo: '/Home'
      });
}]);



